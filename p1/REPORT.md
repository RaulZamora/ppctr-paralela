# P1: Multithreading en C++

Raúl Zamora Martínez

18/12/2018

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VirtualBox:
  - Ubuntu 14.04.4 64bits
  - 2024 MB RAM
  - 2 Cores
 
- Host:
  - CPU intel i5-5200U
  - CPU 4 cores
  - CPU @2200 MHz
  - 8 GiB RAM
  - Trabajo y benchmarks sobre SATA

## 2. Diseño e Implementación del Software
El programa se ha estructurado en un bloque principal, que se encarga de realizar el parseo de los parámetros que se pasan por consola. Durante la ejecución, se marcan unas flags encargadas de indicar qué tipo de operación se va a hacer, y si es en multithreading o no. Además, durante esta etapa se guardan los datos relativos al tamaño del array y el número de threads sobre los que se va a ejecutar el paralelismo.

El programa tiene 4 funciones auxiliares y 2 usadas por el modo multithread. 
- Funciones de cálculo (sum y xor), a las que se les pasan el array sobre el que calcular, y dos índices, de inicio y final (en el modo singlethread se pasa como inicio 0 y como final arraySize). 
- Función 'inicializaArray'. Asigna a cada elemento del array el valor de la posición que ocupa en el mismo.
- Función 'calculaPartición' que calcula el número de elementos del array que deben ir a cada thread. Ajustándose a las especificaciones, la función divide el tamaño del array entre el número de threads pedido. Para esto hay dos variables globales *threadMayor* y *threadNormal*, con los tamaños calculados previamente. 
- Función de hilo, que usa su id y los datos calculados con la función de particiones para saber sobre qué parte del array debe operar.
- Función logger, que recibe los avisos de cada hilo según va terminando.

## 3. Metodología y desarrollo de las pruebas realizadas

Para realizar pruebas sobre el sistema, se han usado múltiples entradas, comprobando todos los posibles casos de error. Tras asegurar que la entrada sea correcta, se ha pasado a la comprobación de que ambos resultados (del modo secuencial y paralelo) coincidan, usando `./practica1 [parámetros] >> touch salida`, y a continuación `diff salidaSecuencial salidaParalela`.

Además, usando la variable del preprocesador DEBUG, se puede ver el valor de todos los atributos del programa que sean relevantes, como las flags y tamaños de particiones o del array, entre otros.

## 4. Discusión

La implementación del logger es parcial, ya que, si bien recibe los avisos por cada thread, no he sido capaz de realizar la gestión de una variable compartida para que su acceso fuese exclusivo por un thread. No obstante, el acceso de los hilos a las regiones críticas, que constarían del acceso a dicha variable compartida, está hecho como he creído que debería ser si el funcionamiento del logger fuese completo.

La gestión del mutex, junto con el paso de directivas de pthreads a las usadas por C++ es lo que más trabajo me ha llevado. A pesar de haber comprendido cuáles son los objetivos esperados todos estos requisitos han ido ralentizando la solución de la práctica.
