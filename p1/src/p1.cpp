#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

/* Variables globales */
int hacer_sum = 0;
int hacer_xor = 0;
int solucionThread[10];
int nThreads;
int threadMayor;
int threadNormal;
int solucionParcial;


std::mutex mut;
std::condition_variable g_c;
bool g_ready = false;

int finalizado;


/* Prototipos de funciones */
int incializaArray (int arraySize, double *array);
double operaSuma (int inicio, int final, double *array);
double operaXor (int inicio, int final, double *array);
int calculaParticion(int arraySize);

void funcionHilo(int iter, double* array);
void funcionLogger();


int main (int argc, char *argv[]) {

	double* array;
	int arraySize = 0;
	int hacer_multiThread = 0;
	double solucion;
	int i;


	/* Comprobación de argumentos */
	for (i = 1; i < argc; i++) {

		char *arg = argv[i];


	if(i == 1) {
		arraySize = atoi(arg);

		if(arraySize == 0) {
			puts("Error de primer parámetro, se espera un entero");
			exit (1);
		}

		array = (double*) malloc(sizeof(double)*arraySize);
		incializaArray(arraySize, array);


		} else if (strcmp (arg, "xor") == 0) {
        	hacer_xor = 1;

        } else if (strcmp (arg, "sum") == 0) {
        	hacer_sum = 1;

        } else if (strcmp (arg, "--multi-thread") == 0) {
        	if(argc != 5) {
        		puts("Se necesita un entero con el parámetro --multi-thread");
        		exit (1);

        	}

        	char *argSig = argv[i+1];
        	nThreads = atoi(argSig);

        	if (nThreads < 1 || nThreads > 10) {
        		puts("Numero de threads incorrecto");
        		exit (1);
        	}

        	hacer_multiThread = 1;
        	calculaParticion(arraySize);

        	i++;

        } else {
        	printf ("Opción no reconocida: %s\n", arg);
         	exit (1);
        }
    }


    /* Parte paralela */
    if(hacer_multiThread == 1) {

    	std::thread t[nThreads];

    	std::thread logger;
    	logger = std::thread(funcionLogger);

    	for (i = 0; i < nThreads; i++)
    	{
    		t[i] = std::thread(funcionHilo, i, array);
    	}

    	for (i = 0; i < nThreads; i++)
    	{
    		t[i].join();
    	}

    	logger.join();
		    	


    } else if(hacer_sum == 1) {
    	solucion = operaSuma(0, arraySize, array);
    	//printf("%lf\n", solucion);

    } else {
    	solucion = operaXor(0, arraySize, array);
    	//printf("%lf\n", solucion);

    }




    if (hacer_sum == 1) {
    	for (i = 0; i < 10; i++) {
    		solucion += solucionThread[i];
    	}
	}

	if (hacer_xor == 1) {
    	solucion = solucionThread[0];
    	for (i = 1; i < 10; i++) {
    		solucion =  (int)solucion ^ solucionThread[i];
    	}
	}

    printf("%lf\n", solucion);


    #ifdef DEBUG
    printf("Tamaño del array: %d\nXOR: %d\nSUM: %d\nMultithread: %d\nNumero de threads: %d\n", arraySize, hacer_xor, hacer_sum, hacer_multiThread, nThreads);
    printf("Elementos thread normal: %d\nElementos thread mayor : %d\n", threadNormal, threadMayor);
    #endif

	return 0;

}


//----------------------------------------------------------------------------------//
/* Función auxiliar que inicializa el array, con la posición de cada índice en double */
int incializaArray (int arraySize, double *array){

	int i;

	for (i = 0; i < arraySize; i++) {
		array[i] = i;
	}

	return 0;
}


//----------------------------------------------------------------------------------//
/* Función que calcula el númerno de elementos del array que deben ir a cada thread */
int calculaParticion(int arraySize) {
	if(nThreads == 0){
		puts("Error de división entre 0");
		exit(1);
	}

	threadNormal = arraySize/nThreads;
	threadMayor = threadNormal + (arraySize%nThreads);

	return 0;
}


//----------------------------------------------------------------------------------//
/* Funcion que calcula la operacion suma sobre un array */
double operaSuma (int inicio, int final, double* array){

	int i;
	double solucion = array[inicio];

	for (i = inicio+1; i < final; i++) {
		solucion = solucion + array[i];
	}

	return solucion;
}


//----------------------------------------------------------------------------------//
/* Función que calcula la operación xor sobre un array */
double operaXor (int inicio, int final, double *array){

	int i;
	double solucion = array[inicio];

	for (i = inicio+1; i < final; i++) {
		solucion = (int)solucion ^ (int)array[i];
	}

	return solucion;
}


//----------------------------------------------------------------------------------//

void funcionHilo(int iter, double* array){

	int posInicio, posFinal;
	posFinal = threadMayor + threadNormal * iter;

	if(iter !=0){
		posInicio = posFinal - threadNormal;		
	} else {
		posInicio = 0;
	}


	if (hacer_sum == 1) {

			std::lock_guard<std::mutex> lock(mut);
			{
				g_ready = true;
				solucionThread[iter] = operaSuma(posInicio, posFinal, array);
				finalizado = iter;

				g_c.notify_one();
			}
		

	} else if (hacer_xor == 1) {

			std::lock_guard<std::mutex> lock(mut);
			{
				g_ready = true;
				solucionThread[iter] = operaXor(posInicio, posFinal, array);
				finalizado = iter;

				g_c.notify_one();
			}
	}


}


void funcionLogger(){

	int i;
	int total[nThreads];

	for(i = 0; i < nThreads; i++){
		std::unique_lock<std::mutex> ulk(mut);
		g_c.wait(ulk, []{return g_ready;});

		//printf("Condicion alcanzada\n");
		total[finalizado] = solucionParcial;
		printf("%d\n", total[finalizado]);

		ulk.unlock();
	}


	sleep(1);

	for (i = 0; i < nThreads; i++)
	{
		printf("Thread logger: %d\n", total[i]);
	}


	
}