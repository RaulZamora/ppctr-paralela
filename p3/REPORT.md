# P3: VideoTask

Raúl Zamora Martínez

21/12/2018

## Prefacio

A diferencia de la práctica anterior, el proceso de benchmarking es menos extenso, pues solo hay que comparar entre dos ejecuciones: la secuencial y la paralela.
Durante la fase de pruebas, se han usado dos programas; el secuencial, que no tiene ningún cambio respecto al original, y el paralelo. El secuencial se ha modificado ligeramente
para que el archivo que genera tenga de nombre *movieSecuencial.out*. Así, se puede usar el comando `diff movie.out movieSecuencial.out` para comprobar que las dos ejecuciones
han filtrado el archivo de igual forma.
Para evitar tiempos de benchmarking desproporcionados, se ha cambiado el archivo **generator.c**. Los tamaños de imagen usados son de 360x240 píxeles, junto con 40 imágenes en vez de 80.
Los tiempos resultantes son suficientemente consistentes como para suponer que estos valores no realizan un sesgo en el speedUp .
En el computador usado para los test, dan una media de 52.774s.


## Desarrollo del Software

Para la realización de la práctica ha sido necesario cambiar el funcionamiento del *do/while*, ya que en su estado inicial impide separar el filtrado de una imagen
como una tarea a repartir entre hilos mientras que el hilo principal va guardando las imágenes y leyendo otras nuevas. Se ha añadido un contador j, para saber cuántas imágenes se han procesado del bloque, 
de esta manera, las guarda también en el fichero *movie.out* cuando no son múltiplo del tamaño de bloque.
Para mostrar por pantalla la imagen que se está procesando, se ha añadido un printf con la función **omp_get_thread_num()**. Para evitar tiempos de ejecución erróneos, durante el benchmarking
se ha comentado esa línea para que la entrada/salida no afecte a los tiempos.

En la región paralela, he usado la etiqueta **firstprivate(i)**, que declara la variable i en el master, y la incializa a 0 (valor que tenía anteriormente). Todas las demás
variables son compartidas, pues solo van a ser de lectura o, en el caso de *numImagen* o *j* de escritura única por el thread master.
Se ha usado la directiva **#pragma omp task** sobre la función de filtrado. Esta directiva crea una tarea y la encola. Cuando haya un thread libre, pasará a ejecutar dicha tarea.
Después del for que va leyendo las imágenes del bloque, se usa la directiva **#pragma omp taskwait**, que crea una barrera explícita. Con esto se evita que se proceda
a la lectura del siguiente bloque de imágenes antes de que se hayan filtrado todas las del bloque actual.

El programa se paraleliza de la siguiente manera: al comenzar el *do/while*, se crea el equipo de threads, que son los que se van a ir encargando de realizar los filtrados, mientras que
es el thread principal (para ello se usa **#pragma omp master**, que hace que sea el main el thread 0) el que lee y escribe de los ficheros *movie.in* y *movie.out* respectivamente.
Durante el filtrado de imágenes, se lleva la cuenta de cuántas van por bloque, para que, en caso de que sea el último bloque del fichero, solo se guarden las que han sido procesadas
por la función gaussiana.

Tras ejecutar varias veces, obtenemos un tiempo medio de 23.721s => speedUp = 52.774/23.721 = 2.224.

A partir de estos resultados vemos que el speedUp no es particularmente mejor para estar siendo ejecutado con 4 threads (el computador tiene dos cores físicos y dos virtuales,
se han modificado los parámetros de la VirtualBox respecto a las prácticas anteriores para intentar ver mejor el efecto del paralelismo). No obstante, se puede suponer que gran
parte del tiempo que no ve mejora está siendo dedicado a lecturas y escrituras sobre los ficheros *movie.in* y *movie.out*, ejecutados ambos por un solo thread de forma secuencial.

