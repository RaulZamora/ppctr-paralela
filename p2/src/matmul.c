#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>

#define RAND rand() % 100

void init_mat_sup();
void init_mat_inf();
void matmul_secuencial();
void matmul();
void matmul_sup_secuencial();
void matmul_sup_static();
void matmul_sup_dynamic();
void matmul_sup_guided();
void matmul_inf();
void print_matrix (float *M, int dim);

/* Usage: ./matmul <dim> [block_size]*/
int block_size = 1;

int main (int argc, char* argv[])
{
	int dim;
	float *A, *B, *C;

	double t1, t2;


   dim = atoi (argv[1]);
   if (argc == 3) block_size = atoi (argv[2]);

	A = (float*) malloc(sizeof(float)*dim*dim);
	B = (float*) malloc(sizeof(float)*dim*dim);
	C = (float*) malloc(sizeof(float)*dim*dim);

	init_mat_sup(dim, A);
	init_mat_inf(dim, B);


	#ifdef BENCHMARKING
	int i;
	int iteraciones = 4;
	double mediaSecuencial, media, speedUp, sumatorio = 0;

	printf("----SECUENCIAL----\n");
	matmul_secuencial(A,B,C,dim); // Ejecución de calentamiento
	for (i = 0; i < iteraciones; i++)
	{
		t1 = omp_get_wtime();
		matmul_secuencial(A, B, C, dim);
		t2 = omp_get_wtime();

		sumatorio += t2-t1;
	}
	mediaSecuencial = sumatorio/iteraciones;
	printf("mediaSecuencial: %lf\n\n", mediaSecuencial);
	sumatorio = 0;



	printf("----PARALELA----\n");
	matmul(A,B,C,dim); // Ejecución de calentamiento
	for (i = 0; i < iteraciones; i++)
	{
		t1 = omp_get_wtime();
		matmul(A, B, C, dim);
		t2 = omp_get_wtime();

		sumatorio += t2-t1;
	}
	media = sumatorio/iteraciones;
	speedUp = media/mediaSecuencial;
	printf("speedUp ejecución paralela: %lf\n\n", speedUp);
	sumatorio = 0;


	printf("----SECUENCIAL SUPERIOR----\n");
	matmul_sup_secuencial(A,B,C,dim); // Ejecución de calentamiento
	for (i = 0; i < iteraciones; i++)
	{
		t1 = omp_get_wtime();
		matmul_sup_secuencial(A, B, C, dim);
		t2 = omp_get_wtime();

		sumatorio += t2-t1;
	}
	mediaSecuencial = sumatorio/iteraciones;
	printf("mediaSecuencial: %lf\n\n", mediaSecuencial);
	sumatorio  = 0;


	printf("----PARALELA SUPERIOR----\n");
	matmul_sup_static(A,B,C,dim); // Ejecución de calentamiento
	for (i = 0; i < iteraciones; i++)
	{
		t1 = omp_get_wtime();
		matmul_sup_static(A, B, C, dim);
		t2 = omp_get_wtime();

		sumatorio += t2-t1;
	}
	media = sumatorio/iteraciones;
	speedUp = media/mediaSecuencial;
	printf("speedUp STATIC: %lf\n", speedUp);
	sumatorio = 0;


	matmul_sup_dynamic(A,B,C,dim); // Ejecución de calentamiento
	for (i = 0; i < iteraciones; i++)
	{
		t1 = omp_get_wtime();
		matmul_sup_dynamic(A, B, C, dim);
		t2 = omp_get_wtime();

		sumatorio += t2-t1;
	}
	media = sumatorio/iteraciones;
	speedUp = media/mediaSecuencial;
	printf("speedUp DYNAMIC: %lf\n", speedUp);
	sumatorio = 0;

	matmul_sup_guided(A,B,C,dim); // Ejecución de calentamiento
	for (i = 0; i < iteraciones; i++)
	{
		t1 = omp_get_wtime();
		matmul_sup_guided(A, B, C, dim);
		t2 = omp_get_wtime();

		sumatorio += t2-t1;
	}
	media = sumatorio/iteraciones;
	speedUp = media/mediaSecuencial;
	printf("speedUp GUIDED: %lf\n", speedUp);
	#endif

	//print_matrix(C, dim);
	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}



void matmul_secuencial (float *A, float *B, float *C, int dim)
{
	int i, j, k;

		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;


		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				for (k=0; k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];

} 


void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	#pragma omp parallel private(i, j, k) shared(A, B, C, dim) num_threads(omp_get_max_threads())
	{
		//int threads = omp_get_num_threads();
		//printf("Threads en la rergion paralela: %d\n", threads);

		#pragma omp for
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				for (k=0; k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];

	}
}

void matmul_sup_secuencial (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	{
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 


void matmul_sup_static (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	#pragma omp parallel private(i, j, k) shared(A, B, C, dim) num_threads(omp_get_max_threads())
	{
		#pragma omp for schedule(static, block_size)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(static, block_size)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 

void matmul_sup_dynamic (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	#pragma omp parallel private(i, j, k) shared(A, B, C, dim) num_threads(omp_get_max_threads())
	{
		#pragma omp for schedule(dynamic, block_size)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(dynamic, block_size)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 


void matmul_sup_guided (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	#pragma omp parallel private(i, j, k) shared(A, B, C, dim) num_threads(omp_get_max_threads())
	{
		#pragma omp for schedule(guided, block_size)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(guided, block_size)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}


void print_matrix (float *M, int dim) {
	int i, j;

	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++){
			fprintf(stdout, "%.1f ", M[i*dim+j]);
		}

		fprintf(stdout, "\n");
	}
	printf("\n");
}
