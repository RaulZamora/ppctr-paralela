# P2: MatMul

Raúl Zamora Martínez 

20/12/2018

# Prefacio

La implementación del programa consta de una sección de benchmarking en el propio código principal (no hay scripts), que se puede activar compilando con `-D BENCHMARKING` como variable del preprocesador.
El benchmarking ha sido estructurado de forma que se pueda ejecutar en cualquier computadora, teniendo en cuenta las especificaciones del mismo:
- Múltiples iteraciones con un cálculo de la media
- Ejecución de calentamiento antes de cada estación
- Cálculo de la media de todos los tipos de ejecución
- Cálculo del speedUp sobre la ejecución secuencial del inicio

La ejecución siempre se realiza sobre los tres tipos de scheduling de omp: static, dynamic y guided. Lo cual ayuda a tener una visión más completa de cómo los 
diferentes resultados se pueden comparar entre sí. Hay un error en el cálculo del speedUp, puesto que hace la división de forma inversa. Sin embargo, simplemente
hay que interpretar los resultados. Si da un speedUp de 0.3, quiere decir que el sistema antiguo es un 30% peor, o lo que es lo mismo, que la nueva forma de hacer la
operación es un 70% mejor.


# Ejercicio 1

##### **1. Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado.**

Las directivas de OpenMP usadas son las siguientes:

**#pragma omp parallel** Crea una región paralela, sobre la que actuarán un equipo de threads. En este caso viene definido por la cláusula **num_threads()** a la
que se añade la función **omp_get_max_threads()** encargada de saber cuál es el número máximo de threads que puede usar el sistema (en este caso 2 threads, uno por core).

**#pragma omp for** Divide la ejecución de un for ubicado a continuación entre los threads de la región paralela.

Se ha usado la función **omp_get_wtime()**, para obtener los tiempos relativos al benchmarking. Esta función devuelve el tiempo de ejecución en segundos hasta ese
ese punto del programa. Para caulcular un tiempo de ejecución hacen falta dos variables de control al inicio y final de la zona en cuestión, para luego restarlas.

###### **2. Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa etiqueta.**

Se han usado dos tipos de etiquetas:

**private(i, j, k)** Estas variables son las usadas por los for anidados para realizar las iteraciones, si no se declaran como privadas se pisarían entre threads
por lo que no podrían llevar la cuenta de qué iteración toca.

**shared(A, B, C, dim)** Las variables de los vectores A, B y C solo van a ser accedidas o escritas en posiciones únicas, por lo que pueden ser compartidas, de la 
misma manera la variable dim es de solo lectura, por lo que se aplica la misma justificación.

###### **3. Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo.**

Al iniciar la función matmul, se crea una región paralela con el número de threads máximo disponible por el sistema. Antes de comenzar el primer bloque de iteraciones
sobre los vectores, se usa el pragma for, lo que reparte la ejecución de dicho for entre los threads creados anteriormente. Al no especificar la forma en que las iteraciones 
se tienen que dividir entre los threads, esto se resuelve por el compilador o en tiempo de ejecución. 

##### **4. Calcula la ganancia (speedup) obtenido con la paralelización.**

Usando los resultados del benchmark, obtenemos que la ejecución paralela tiene un speedUp de 1.653, lo que significa que la paralelización mejora el rendimiento en un
65% aproximadamente. Este resultado ha sido consistente al ejecutar el benchmarking en los ordenadores del laboratorio, que dan un speedUp similar.


# Ejercicio 2

##### **1. ¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida sin la clausula schedule? Explica con detalle a qué se debe esta diferencia.**

El tiempo obtenido es el mismo que sin paralelizar, esto puede deberse a que el cálculo optimizado genera opeaciones más costosas de calcular al inicio que al final, 
y el modo que se usa sin la clausula suele ser static. Como la repartición es igual entre todos los threads, no está teniendo en cuenta que son las primeras 
iteraciones las que están haciendo la mayor parte del trabajo.

##### **2. ¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado? Explica por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las operaciones que tiene que realizar cada thread.**

El mejor speedUp se obtiene con el algoritmo dynamic, con un speedUp de 1.512 (aproximadamente un 50% mejor que la ejecución secuencial). Este algoritmo divide la carga de trabajo entre los threads
(por defecto en bloques de 1 iteración) y se va asignando nuevos bloques según finalizan su trabajo. Aunque un thread tarde mucho en realizar la iteración que se le ha asignado, los
demás pueden estar realizando iteraciones del final (o también iteraciones complejas) con lo que el trabajo se va repartiendo sin ser 'amontonado' en un solo thread, que es lo que evita mejoras.

##### **3. Para el algoritmo static piensa cuál será el tamaño del bloque óptimo, en función de cómo se reparte la carga de trabajo.**

Por los motivos dichos anteriormente, el mejor tamaño de bloque será 1, lo que permite evitar apilar iteraciones con mucha carga computacional en un solo thread.
El speedUp opbtenido es de 1.655 (65% mejor). Es mejor que el dynamic porque evita la planificación en tiempo de ejecución, es decir no añade tanto overhead.

##### **4. Para el algoritmo dynamic determina experimentalmente el tamaño del bloque óptimo. Para ello mide el tiempo de respuesta y speedup obtenidos para al menos 10 tamaños de bloque diferentes. Presenta una tabla resumen de los resultados y explicar detalladamente estos resultados.**

| Tamaño de bloque | SpeedUp(en %) |
|------------------|---------------|
| 1                | 51%           |
| 50               | 64%           |
| 100              | 57%           |
| 150              | 55%           |
| 200              | 49%           |
| 300              | 12%           |
| 500              | -2%           |

El tamaño mejor se encuentra en la región de tamaño de bloque [50, 100]. Esto puede variar ligeramente entre un computador y otro, pero en ambas ejecuciones en el portátil
y en los ordenadores del laboratorio los resultados han sido similares. A partir de 250-300, el speedUp decae rápidamente y sobre más de 400-500 empieza a ser peor
la ejecución paralela que la secuencial.

De forma similar que en los casos anteriores, al hacer el tamaño del bloque demasiado grande, las iteraciones más costosas se apilan en un solo thread, por lo que no se
gana frente a la ejecución secuencial. Para tamaños menores a 50, ocurre lo mismo descrito en la pregunta anterior, el tiempo que se gana en el reparto de trabajo
se pierde en el tiempo que tarda el planificador en gestionar los hilos.

##### **5. Explica los resultados del algoritmo guided, en función del reparto de carga de trabajo que realiza.**

El algoritmo guided va repartiendo el tamaño de bloque incialmente con tamaños muy grandes, para luego ir disminuyendo los mismos. Vuelve a suceder el mismo problema que antes
ya que, a pesar de que la ejecución de la parte final es más rápida, pues los threads tienen pocas iteraciones cada uno con carga computacional muy baja, al principio el tamaño de 
bloque es grande, por lo que la mayor parte del trabajo está recayendo sobre uno o dos threads de golpe.

## Ejercicio 3

**Analiza la función matmul_inf y comprende su comportamiento. Puedes contestar a las preguntas sin tener que modificar código, ejecutar y medir**

El funcionamiento es el mismo, solo que invertido. La mayor carga de trabajo se encuentra en las últimas iteraciones de los bucles. Teniendo en cuenta los resultados 
del ejercicio anterior, se puede suponer que el mejor algoritmo vaya a ser el guided, ya que los bloques más grandes iniciales van a tener poca carga computacional, y según
se vaya acercando al final los bloques se irán haciendo más pequeños. El algorirmo static tendrá el mismo problea que antes, por lo que seguirá siendo peor. Habrá que especificar tamaño de bloque 1
para que cuando llegue a los últimos bloques no haya un thread donde se acumule todo el trabajo. El algoritmo dynamic no se debería ver afectado por el cambio, sus resultados deberían ser similares a los anteriores.